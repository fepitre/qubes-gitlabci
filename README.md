qubes-gitlabci
===

Standard `.gitlab-ci.yml` can be added or configured into repository to point to `gitlab-component.yml` for both `dom0`
and `vm` builds. Alternatively, `gitlab-component-XXX.yml` can be used fo `XXX` builds where `XXX` is `dom0` or `vm`.

When specific configuration is needed per component, e.g. excluding packages at `install` stage, a `qubes-COMPONENT.yml` is created.

`gitlab-base.yml` defines global variables, stages and template jobs.